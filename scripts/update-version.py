import json
import argparse
import re

parser = argparse.ArgumentParser(description='Read version from package.json and put into .NET/C# App.config.')
parser.add_argument('package_json')
parser.add_argument('app_config')
args = parser.parse_args()

package_json = []

with open(args.package_json) as json_file:
    package_json = json.load(json_file)

app_config = []

with open(args.app_config) as app_config_file:
    for line in app_config_file:
        line = re.sub(r'^\s*<add\skey=\"version\"\svalue=\"([0-9\.]+)\"\s/>$', '<add key="version" value="' + package_json.get('version') + '" />', line.rstrip())
        app_config.append(line)

with open(args.app_config, 'w') as app_config_file:
    for line in app_config:
        app_config_file.write(line + '\n')
