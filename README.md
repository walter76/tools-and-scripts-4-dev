# Windows PowerShell

You need to install:

* [posh-git](https://github.com/dahlbyk/posh-git)
* [Get-ChildItemColor](https://github.com/joonro/Get-ChildItemColor)

# Manjaro Linux Dev Environment

* [Install ZSH on Linux and make it your default Shell](https://medium.com/tech-notes-and-geek-stuff/install-zsh-on-arch-linux-manjaro-and-make-it-your-default-shell-b0098b756a7a)
  * Do not install oh-my-zsh with Manjaro Package manager. Use installation on oh-my-zsh [repo](https://github.com/robbyrussell/oh-my-zsh) instead
* Install [Tilix Terminal Emulator](https://gnunn1.github.io/tilix-web/)

Mounting filesystem: [How To Automount File Systems on Linux](https://www.linuxbabe.com/desktop-linux/how-to-automount-file-systems-on-linux)

## Troubleshooting

If gitlab is not loading with error 422, check if your time settings are correct and `ntp` is synching.

### Create diagnostic info

```sh
inxi -Fazy
efibootmgr -v
parted -l
blkid
cat /etc/fstab
cat /etc/default/grub
```
