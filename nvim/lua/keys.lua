-- [[ keys.lua ]]
local map = vim.api.nvim_set_keymap

-- lsp key mappings
map('n', '<leader>h', [[:lua vim.lsp.buf.hover()<CR>]], {})
map('n', '<leader>gD', [[:lua vim.lsp.buf.declaration()<CR>]], {})
map('n', '<leader>rn', [[:lua vim.lsp.buf.rename()<CR>]], {})
map('n', '<leader>dn', [[:lua vim.lsp.diagnostic.goto_next()<CR>]], {})
map('n', '<leader>dN', [[:lua vim.lsp.diagnostic.goto_prev()<CR>]], {})
map('n', '<leader>xx', [[:lua vim.lsp.buf.code_action()<CR>]], {})

-- those actions below don't work; have to look it up in rust-tools documentation
-- seems to be a problem with Telescope and rust-tools connection
map('n', '<leader>xd', [[:Telescope lsp_range_code_actions]], {})

-- dap key mappings (debugging)
map('n', '<F5>', [[:lua require('dap').continue()<CR>]], {})
map('n', '<F10>', [[:lua require('dap').step_over()<CR>]], {})
map('n', '<F11>', [[:lua require('dap').step_into()<CR>]], {})
map('n', '<F12>', [[:lua require('dap').step_out()<CR>]], {})
map('n', '<leader>b', [[:lua require('dap').toggle_breakpoint()<CR>]], {})
map('n', '<leader>dr', [[:lua require('dap').repl.open()<CR>]], {})
map('n', '<leader>du', [[:lua require('dapui').toggle()<CR>]], {})

local wk = require('which-key')

wk.register({
  ["<leader>"] = {
    n = { "<cmd>Telescope file_browser<cr>", "show/hide file browser" },
    l = { "<cmd>IndentLinesToggle<cr>", "show/hide lines lndentation" },
    t = { "<cmd>TagbarToggle<cr>", "show/hide code structure" },
    f = {
      name = "find",
      f = { "<cmd>Telescope find_files<cr>", "find in files" },
      g = { "<cmd>Telescope live_grep<cr>", "live grep" },
      b = { "<cmd>Telescope buffers<cr>", "find buffers" },
      h = { "<cmd>Telescope help_tags<cr>", "help tags" },
      u = { "<cmd>Telescope lsp_references<cr>", "find usages" },
      d = { "<cmd>Telescope lsp_definitions<cr>", "find definitions" },
      D = { "<cmd>Telescope diagnostics<cr>", "workspace diagnostics" },
    },
  },
})
