-- [[ plug.lua ]]

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'         -- so packer can update itself

  -- [[ Common ]]
  use {
    'nvim-telescope/telescope-file-browser.nvim',
    requires = {
      'nvim-telescope/telescope.nvim',
      'nvim-lua/plenary.nvim',
      'kyazdani42/nvim-web-devicons'
    }
  }

  use {                                -- tab bar
    'romgrk/barbar.nvim',
    requires = {'kyazdani42/nvim-web-devicons'}
  }
  use { 'folke/which-key.nvim' }

  -- [[ Theme ]]
  use { 'mhinz/vim-startify' }                         -- start screen
  use { 'DanilaMihailov/beacon.nvim' }                 -- cursor jump
  use { 'nvim-lualine/lualine.nvim' }                  -- statusline
  use { 'Mofiqul/dracula.nvim' }                       -- color scheme

  -- [[ Writing ]]

  use {
    'iamcco/markdown-preview.nvim',
    run = 'cd app && npm install',
    setup = function() vim.g.mkdp_filetypes = { 'markdown' } end,
    ft = { 'markdown' }
  }

  -- [[ Dev ]]
  use {
    'nvim-telescope/telescope.nvim',                   -- fuzzy finder / nice interface for LSP functions
    requires = { { 'nvim-lua/plenary.nvim' } }
  }
  use { 'majutsushi/tagbar' }          -- code structure
  use { 'Yggdroot/indentLine' }        -- see indentation
  use { 'tpope/vim-fugitive' }         -- git integration
  use { 'junegunn/gv.vim' }            -- commit history
  use { 'windwp/nvim-autopairs' }      -- auto close brackets, etc.
  use {
    'folke/todo-comments.nvim',        -- todo comments
    requires = { { 'nvim-lua/plenary.nvim' } }
  }

  -- [[ Language Support ]]
  use { 'williamboman/mason.nvim' }
  use { 'williamboman/mason-lspconfig.nvim' }

  use 'neovim/nvim-lspconfig'                          -- native LSP support

  use { 'simrat39/rust-tools.nvim' }

  use 'hrsh7th/nvim-cmp'                               -- autocompletion framework
  use 'hrsh7th/cmp-nvim-lsp'                           -- LSP automcompletion provider

  use 'mfussenegger/nvim-lint'

  -- useful completion sources
  use { 'hrsh7th/cmp-nvim-lua' }
  use { 'hrsh7th/cmp-nvim-lsp-signature-help' }
  use { 'hrsh7th/cmp-vsnip' }
  use { 'hrsh7th/cmp-path' }
  use { 'hrsh7th/cmp-buffer' }
  use { 'hrsh7th/vim-vsnip' }

  -- [[ Debugging ]]
  use { 'nvim-lua/plenary.nvim' }
  use { 'mfussenegger/nvim-dap' }
  use {
    'rcarriga/nvim-dap-ui',
    requires = { 'mfussenegger/nvim-dap' }
  }
end)

