-- [[ vars.lua ]]

local g = vim.g
g.t_co = 256
g.background = "dark"

g.indentLine_setConceal = 2
g.indentLine_concealcursor = 'nv'

-- Update the packpath
local packer_path = vim.fn.stdpath('config') .. '/site'
vim.o.packpath = vim.o.packpath .. ',' .. packer_path

