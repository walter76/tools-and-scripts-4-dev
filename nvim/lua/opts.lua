-- [[ opts.lua ]]
local opt = vim.opt
local cmd = vim.api.nvim_command

-- [[ Context ]]
opt.colorcolumn = '100'                -- str:  Show col for max line length
opt.number = true                      -- bool: Show line numbers
opt.relativenumber = true              -- bool: Show relative line numbers
opt.scrolloff = 4                      -- int:  Min num lines of context
opt.signcolumn = "yes"                 -- str:  Show the sign column

-- [[ Filetypes ]]
opt.encoding = 'utf8'             -- str:  String encoding to use
opt.fileencoding = 'utf8'         -- str:  File encoding to use

-- [[ Theme ]]
opt.syntax = "ON"                      -- str:  Allow syntax hightlighting
opt.termguicolors = true               -- bool: If term supports ui color then enable
cmd('colorscheme dracula')             -- cmd:  Set the colorscheme

-- [[ Search ]]
opt.ignorecase = true                  -- bool: Ignore case in search patterns
opt.smartcase = true                   -- bool: Override ignorecase if search contains capitals
opt.incsearch = true                   -- bool: Use incremental search
opt.hlsearch = true                    -- bool: Highlight search matches

-- [[ Whitespace ]]
opt.expandtab = true                   -- bool: Use spaces instead of tabs
opt.shiftwidth = 4                     -- num:  Size of an indent
opt.softtabstop = 4                    -- num:  Number of spaces tabs count for in insert mode
opt.tabstop = 4                        -- num:  Number of spaces tabs count for

-- [[ Splits ]]
opt.splitright = true                  -- bool: Place new window to right of current one
opt.splitbelow = true                  -- bool: Place new window below the current one

-- LSP Diagnostics Options Setup
local sign = function(opts)
  vim.fn.sign_define(opts.name, {
    texthl = opts.name,
    text = opts.text,
    numhl = ''
  })
end

sign({name = 'DiagnosticSignError', text = ''})
sign({name = 'DiagnosticSignWarn', text = ''})
sign({name = 'DiagnosticSignHint', text = ''})
sign({name = 'DiagnosticSignInfo', text = ''})

vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  update_in_insert = true,
  underline = true,
  severity_sort = false,
  float = {
    border = 'rounded',
    source = 'always',
    header = '',
    prefix = '',
  },
})

-- Set completeopt to have a better completion experience
--   :help completeopt
-- menuone: popup even when there's only one match
-- noinsert: Do not insert text until a selection is made
-- noselect: Do not select, force to select one from the menu
-- shortmess: avoid showing extra messages when using completion
-- updatetime: set updatetime for CursorHold
vim.opt.completeopt = { 'menuone', 'noselect', 'noinsert' }
vim.opt.shortmess = vim.opt.shortmess + { c = true }
vim.api.nvim_set_option('updatetime', 300)

-- Fixed column for diagnostics to appear
-- Show autodiagnostic popup on cursor hover_range
-- Goto previous / next diagnostic warning / error
-- Show inlay_hints more frequrently
vim.cmd([[
set signcolumn=yes
autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })
]])

-- Treesitter folding
-- vim.wo.foldmethod = 'expr'
-- vim.wo.foldexpr = 'nvim_treesitter#foldexpr()'

