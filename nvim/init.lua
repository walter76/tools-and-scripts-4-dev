-- [[ init.lua ]]

-- LEADER
-- These keybindings need to be defined before the first /
-- is called; otherwise, it will default to "\"
vim.g.mapleader = ","
vim.g.localleader = "\\"

-- IMPORTS
require('vars')      -- Variables
require('opts')      -- Options
require('keys')      -- Keymaps
require('plug')      -- Plugins

-- PLUGINS
require('telescope').setup{}
require('telescope').load_extension('file_browser')

require('lualine').setup {
  options = {
    theme = 'dracula-nvim'
  }
}

require('which-key').setup{}

require('nvim-autopairs').setup{}

require('todo-comments').setup{}

-- autocomplete config
local cmp = require('cmp')
cmp.setup {
  -- enable LSP snippets
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },

  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),

    -- add Tab support
    ['<S-Tab>'] = cmp.mapping.select_prev_item(),
    ['<Tab>'] = cmp.mapping.select_next_item(),

    ['<C-S-f>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    })
  },

  -- installed sources:
  sources = {
    { name = 'path' },                          -- file paths
    { name = 'nvim_lsp', keyword_length = 3 },  -- from language server
    { name = 'nvim_lsp_signature_help' },       -- display function signatures with current parameter emphasized
    { name = 'nvim_lua', keyword_length = 2 },  -- complete neovim's Lua runtime API such vim.lsp.*
    { name = 'buffer', keyword_length = 2 },    -- source current buffer
    { name = 'vsnip', keyword_length = 2 },     -- nvim-cmp source vim-vsnip
    { name = 'calc' },                          -- source for math calculation
  },

  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },

  formatting = {
    fields = {'menu', 'abbr', 'kind'},
    format = function(entry, item)
      local menu_icon = {
        nvim_lsp = 'λ',
        vsnip = '⋗',
        buffer = 'Ω',
        path = '🖫',
      }
      item.menu = menu_icon[entry.source.name]
      return item
    end
  },
}

require('mason').setup {
  -- log_level = vim.log.levels.TRACE,
}

require('mason-lspconfig').setup()

local lspconfig = require('lspconfig')

-- omnisharp lsp config
local capabilities = require('cmp_nvim_lsp').default_capabilities()
local pid = vim.fn.getpid()

require ('lspconfig').omnisharp.setup {
  capabilities = capabilities,
  on_attach = function(_, bufnr)
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  end,
  -- windows only
  -- cmd = {
  --   "C:\\home\\bin\\omnisharp\\net6\\OmniSharp.exe", "--languageserver", "--hostPID", tostring(pid)
  -- },
}

local dap = require('dap')
dap.adapters.codelldb = {
  type = 'server',
  port = "${port}",
  executable = {
    command = 'codelldb',
    args = {"--port", "${port}"},

    -- On windows you may have to uncomment this:
    -- detached = false,
  }
}

dap.configurations.rust = {
  {
    name = "Launch file",
    type = "codelldb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
  }
}

require('dapui').setup()

-- Rust LSP Setup
local rt = require('rust-tools')
rt.setup({
--  tools = {
--    runnables = {
--      use_telescope = true,
--    }
--  },
  server = {
    on_attach = function(_, bufnr)
      -- Hover actions
      vim.keymap.set("n", "<C-Space>", rt.hover_actions.hover_actions, { buffer = bufnr })
      -- Code action groups
      vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
    end,
    -- windows only
    -- cmd = {
    --   "C:\\home\\bin\\rust-analyzer\\rust-analyzer.exe"
    -- },
  },
})

-- lua LSP setup
lspconfig.lua_ls.setup {
  settings = {
    Lua = {
      runtime = {
        -- Tell the lanuage server which version of Lua you're using (most likely LuaJIT in the
        -- case of Neovim)
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { 'vim' },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      }
    }
  }
}

-- python LSP setup
lspconfig.pylsp.setup {}

-- typescript / javascript LSP setup
lspconfig.tsserver.setup {
  init_options = {
    preferences = {
      disableSuggestions = true,
    }
  }
}

require('lint').linters_by_ft = {
  javascript = { 'eslint' },
  typescript = { 'eslint' }
}

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  callback = function()
    require('lint').try_lint()
  end,
})

