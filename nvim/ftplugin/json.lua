-- [[ Whitespace ]]
vim.opt_local.expandtab = true                   -- bool: Use spaces instead of tabs
vim.opt_local.shiftwidth = 2                     -- num:  Size of an indent
vim.opt_local.softtabstop = 2                    -- num:  Number of spaces tabs count for in insert mode
vim.opt_local.tabstop = 2                        -- num:  Number of spaces tabs count for

