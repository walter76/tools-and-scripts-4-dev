# VS Code Extensions

![](./extensions_01.png)
![](./extensions_02.png)

* [.NET Core Test Explorer](https://marketplace.visualstudio.com/items?itemName=formulahendry.dotnet-test-explorer)
* Auto Close Tag - bad rating. Is there an alternative?
* [Auto Import](https://marketplace.visualstudio.com/items?itemName=steoates.autoimport)
* [Babel JavaScript](https://marketplace.visualstudio.com/items?itemName=mgmcdermott.vscode-language-babel)
* [Better TOML](https://open-vsx.org/extension/bungcip/better-toml)
* C/C++ - bundled with VSCode
* [C#](https://open-vsx.org/extension/muhammad-sammy/csharp)
* C# Snippets - ?
* [Code Spell Checker](https://open-vsx.org/extension/streetsidesoftware/code-spell-checker)
* CodeLLDB - ?
* [Debugger for Chrome](https://open-vsx.org/extension/msjsdiag/debugger-for-chrome)
* [Debugger for Firefox](https://open-vsx.org/extension/firefox-devtools/vscode-firefox-debug)
* DevTools for Chrome - ?
* [Docker](https://open-vsx.org/extension/ms-azuretools/vscode-docker)
* [DotEnv](https://open-vsx.org/extension/mikestead/dotenv)
* [Dracula Official](https://open-vsx.org/extension/dracula-theme/theme-dracula)
* [ES7 React/Redux/GraphQL/React-Native snippets](https://open-vsx.org/extension/dsznajder/es7-react-js-snippets)
* [ESLint](https://open-vsx.org/extension/dbaeumer/vscode-eslint)
* [Git History](https://open-vsx.org/extension/donjayamanne/githistory)
* [GitLens - Git supercharged](https://open-vsx.org/extension/eamodio/gitlens)
* Graphviz (dot) - ?
* Jupyter - ?
* [Markdown All in One](https://open-vsx.org/extension/yzhang/markdown-all-in-one)
* Native Debug - ?
* npm - ?
* npm Intellisense - ?
* [Prettier - Code formatter](https://open-vsx.org/extension/esbenp/prettier-vscode)
* [Python](https://open-vsx.org/extension/ms-python/python)
* [Todo Tree](https://open-vsx.org/extension/Gruntfuggly/todo-tree)
* TOML Language Support - ?
* [vscode-icons](https://open-vsx.org/extension/vscode-icons-team/vscode-icons)
* [vscode-proto3](https://open-vsx.org/extension/zxh404/vscode-proto3)
* 